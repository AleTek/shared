1
00:00:00,471 --> 00:00:04,162
(upbeat music)

2
00:00:04,162 --> 00:00:06,122
- [Voiceover] Well, that brings
us to the end of our first

3
00:00:06,122 --> 00:00:09,642
module in this JavaEE with Wildfly course.

4
00:00:09,642 --> 00:00:11,842
We've covered quite a lot in this module.

5
00:00:11,842 --> 00:00:15,633
We started by talking about
what JavaEE is and what an

6
00:00:15,633 --> 00:00:18,213
application server is designed to do.

7
00:00:18,213 --> 00:00:20,433
Then we installed and
configured the Wildfly

8
00:00:20,433 --> 00:00:22,393
application server.

9
00:00:22,393 --> 00:00:26,073
Next, we created our first
EJB, or Enterprise Java Bean,

10
00:00:26,073 --> 00:00:29,731
which is the basic building
block of JavaEE applications

11
00:00:29,731 --> 00:00:32,831
and we deployed the EJB to the server.

12
00:00:32,831 --> 00:00:35,691
Next we created a simple
client application to call

13
00:00:35,691 --> 00:00:37,911
a method on the EJB.

14
00:00:37,911 --> 00:00:41,151
Then we talked about
connecting EJB's together,

15
00:00:41,151 --> 00:00:43,251
using dependency injection.

16
00:00:43,251 --> 00:00:46,931
And we learned how you can
switch EJB is injected,

17
00:00:46,931 --> 00:00:50,791
either with the beans.xml
file or by using qualifier

18
00:00:50,791 --> 00:00:52,411
annotations.

19
00:00:52,411 --> 00:00:55,591
We then moved on to learning
about database functionality

20
00:00:55,591 --> 00:01:00,591
and we used JPA to integrate
the database into our project.

21
00:01:00,871 --> 00:01:03,431
We spent quite some time
understanding how transactions

22
00:01:03,431 --> 00:01:05,671
and rollbacks work.

23
00:01:05,671 --> 00:01:07,731
Throughout this module
we've had a very simple case

24
00:01:07,731 --> 00:01:10,571
study, but using this, we've
been able to build some

25
00:01:10,571 --> 00:01:13,111
production quality, fully functioning,

26
00:01:13,111 --> 00:01:14,751
end-to-end code.

27
00:01:14,751 --> 00:01:17,631
You may want to continue
your practice with JavaEE

28
00:01:17,631 --> 00:01:20,011
by expanding the code that
we've written to include

29
00:01:20,011 --> 00:01:22,871
functions to update or delete employees.

30
00:01:22,871 --> 00:01:24,931
I'll leave that for you to decide.

31
00:01:24,931 --> 00:01:28,311
As this module goes live,
I'm going to start working

32
00:01:28,311 --> 00:01:31,531
on the next module, which
will cover webservices

33
00:01:31,531 --> 00:01:32,931
with SOAP and REST.

34
00:01:32,931 --> 00:01:35,491
Two of the more modern
ways to build client server

35
00:01:35,491 --> 00:01:39,231
applications and we'll also
learn about message queue's

36
00:01:39,231 --> 00:01:40,611
in module two.

37
00:01:40,611 --> 00:01:44,151
And then, module three will
finish the course by covering

38
00:01:44,151 --> 00:01:46,151
webpages and security.

39
00:01:46,151 --> 00:01:48,591
I'm not going to give a
date on when these modules

40
00:01:48,591 --> 00:01:51,831
will be available, as we do
our best to respond to what

41
00:01:51,831 --> 00:01:53,791
our customers are asking for.

42
00:01:53,791 --> 00:01:57,331
So keep an eye on the
Virtual Programmers website

43
00:01:57,331 --> 00:01:59,531
for details of when they're
going to be released.

44
00:01:59,531 --> 00:02:02,591
If you're new to JPA, then
I strongly suggest that you

45
00:02:02,591 --> 00:02:07,011
study the hibernate and JPA
course we have available at

46
00:02:07,011 --> 00:02:09,471
Virtual Pair Programmers
as your next step.

47
00:02:09,471 --> 00:02:13,351
You might also find the Java
build tools course useful also.

48
00:02:13,351 --> 00:02:16,731
But for now, thank you for
watching and good luck.

49
00:02:16,731 --> 00:02:19,621
(upbeat music)

