1
00:00:08,073 --> 00:00:10,108
- [Voiceover] We are now going to install

2
00:00:10,108 --> 00:00:13,868
and configure the Wildfly
application server.

3
00:00:13,868 --> 00:00:16,440
Before we start, I'm
assuming that you have

4
00:00:16,440 --> 00:00:18,479
a working operating system

5
00:00:18,479 --> 00:00:20,071
with at least version 8

6
00:00:20,071 --> 00:00:22,961
of the Java Development Kit installed.

7
00:00:22,961 --> 00:00:26,184
Now, I'm using the Ubuntu
Linux operating system

8
00:00:26,184 --> 00:00:28,262
in this course, but you can follow along

9
00:00:28,262 --> 00:00:32,018
with Windows or Mac or
any other flavor of Linux.

10
00:00:32,018 --> 00:00:34,664
Everything that we do will
be identical no matter

11
00:00:34,664 --> 00:00:37,576
what the operating system
is that you're using

12
00:00:37,576 --> 00:00:39,624
or if it is something that's different,

13
00:00:39,624 --> 00:00:41,610
I will show you what you need to do

14
00:00:41,610 --> 00:00:43,128
for your operating system.

15
00:00:43,128 --> 00:00:45,532
Now you need to have installed a JDK,

16
00:00:45,532 --> 00:00:48,872
or Java Development
Kit, of at least Java 8

17
00:00:48,872 --> 00:00:52,168
to be able to run the
latest versions of Wildfly.

18
00:00:52,168 --> 00:00:54,908
If you try and run with
an earlier version,

19
00:00:54,908 --> 00:00:57,111
you'll get an error on start up

20
00:00:57,111 --> 00:01:00,664
as the configuration files
which ship with Wildfly

21
00:01:00,664 --> 00:01:03,031
set some virtual machine options

22
00:01:03,031 --> 00:01:06,165
which were only introduced in Java 8.

23
00:01:06,165 --> 00:01:09,156
So to check which version
of Java you have installed,

24
00:01:09,156 --> 00:01:12,266
let's open a terminal
window or a command prompt

25
00:01:12,266 --> 00:01:14,913
if you're using a Windows machine,

26
00:01:14,913 --> 00:01:19,913
and then we'll type in
here, javac -version.

27
00:01:20,334 --> 00:01:21,650
So as you can see here,

28
00:01:21,650 --> 00:01:23,151
I'm using Java 8.

29
00:01:23,151 --> 00:01:24,914
That shows as 1.8.

30
00:01:24,914 --> 00:01:29,038
Hopefully, you've got something
similar in your output.

31
00:01:29,038 --> 00:01:32,386
If you need to install
the Java Development Kit,

32
00:01:32,386 --> 00:01:36,700
note that it's the
standard Java SE version

33
00:01:36,700 --> 00:01:38,776
that you need to install.

34
00:01:38,776 --> 00:01:41,641
If you're downloading it
from the Oracle website,

35
00:01:41,641 --> 00:01:44,159
make sure you click on the Java SE

36
00:01:44,159 --> 00:01:48,123
and then the Downloads
tab to obtain from here.

37
00:01:48,123 --> 00:01:49,555
So, hopefully, you've done that.

38
00:01:49,555 --> 00:01:52,944
You've got a version of
the Java JDK installed

39
00:01:52,944 --> 00:01:55,219
and the next step then is to download

40
00:01:55,219 --> 00:01:57,965
and install the Wildfly software.

41
00:01:57,965 --> 00:02:00,780
To get it, we need to
go the Wildfly website,

42
00:02:00,780 --> 00:02:05,303
which is www.wildfly.org.

43
00:02:05,303 --> 00:02:07,559
Then click on the downloads link

44
00:02:07,559 --> 00:02:10,331
and at the time of writing this course,

45
00:02:10,331 --> 00:02:15,331
the latest version of Wildfly is 10.0.0.

46
00:02:15,428 --> 00:02:19,025
Now this uses Java EE7.

47
00:02:19,025 --> 00:02:20,781
You should be fine to do this course

48
00:02:20,781 --> 00:02:23,189
on any future version of Wildfly

49
00:02:23,189 --> 00:02:25,378
which also uses Java EE7.

50
00:02:25,378 --> 00:02:28,486
I guess eventually, there'll be a Java EE8

51
00:02:28,486 --> 00:02:30,360
or 9 version available

52
00:02:30,360 --> 00:02:32,267
and sometimes the changes

53
00:02:32,267 --> 00:02:34,680
between these versions are significant.

54
00:02:34,680 --> 00:02:36,640
So where there are changes which means

55
00:02:36,640 --> 00:02:39,206
that something on this course
is not working anymore,

56
00:02:39,206 --> 00:02:41,855
I will put the detail of what is changed

57
00:02:41,855 --> 00:02:43,754
in the errata for this course,

58
00:02:43,754 --> 00:02:47,111
which you'll find on the
course page on our website.

59
00:02:47,111 --> 00:02:50,905
But for now, note that
the Java EE versions

60
00:02:50,905 --> 00:02:53,189
are a little bit behind
the latest versions

61
00:02:53,189 --> 00:02:54,776
of standard Java.

62
00:02:54,776 --> 00:02:57,111
You'll notice that there
are a few different versions

63
00:02:57,111 --> 00:03:00,078
of the Java EE server.

64
00:03:00,078 --> 00:03:02,818
Part of the Java EE specification

65
00:03:02,818 --> 00:03:06,524
is that it includes
profiles that are a part

66
00:03:06,524 --> 00:03:08,487
of the full spec.

67
00:03:08,487 --> 00:03:11,999
If, for example, you were only
going to be using servlets,

68
00:03:11,999 --> 00:03:15,098
then rather than getting
an application server,

69
00:03:15,098 --> 00:03:19,826
which has the whole set of Java
EE library implementations,

70
00:03:19,826 --> 00:03:23,372
you could just download
the servlet version.

71
00:03:23,372 --> 00:03:27,250
This is a much smaller file
and it will be quicker to run.

72
00:03:27,250 --> 00:03:30,431
However, in this course, we'll
be learning about a number

73
00:03:30,431 --> 00:03:33,158
of different ways of using Java EE

74
00:03:33,158 --> 00:03:36,468
so we will need the full distribution.

75
00:03:36,468 --> 00:03:38,763
So let's download and install this

76
00:03:38,763 --> 00:03:40,962
and no matter whether you're using Windows

77
00:03:40,962 --> 00:03:43,817
or Mac or Linux, you
need to do the same thing

78
00:03:43,817 --> 00:03:46,347
which is to download the zip file

79
00:03:46,347 --> 00:03:48,595
using this link here.

80
00:03:49,731 --> 00:03:51,400
OK, so that's downloaded.

81
00:03:51,400 --> 00:03:53,284
Here it is in my Downloads folder

82
00:03:53,284 --> 00:03:55,523
and the next thing we
need to do is unzip it.

83
00:03:55,523 --> 00:03:58,506
So I can do that by double
clicking on the file

84
00:03:58,506 --> 00:04:00,220
and then in this version of Ubuntu,

85
00:04:00,220 --> 00:04:02,508
I need to click on the Extract button.

86
00:04:02,508 --> 00:04:04,505
That'll be something
similar to do, obviously,

87
00:04:04,505 --> 00:04:07,605
in your version of your operating system.

88
00:04:07,605 --> 00:04:09,064
So that's now completed.

89
00:04:09,064 --> 00:04:11,035
Here's the folder that was created

90
00:04:11,035 --> 00:04:13,155
and we now need to put
this somewhere sensible

91
00:04:13,155 --> 00:04:14,707
on our computer.

92
00:04:14,707 --> 00:04:16,175
If you're using a Mac,

93
00:04:16,175 --> 00:04:18,707
you might want to put it
in the Applications folder.

94
00:04:18,707 --> 00:04:20,501
On Windows, you'll
probably choose to put it

95
00:04:20,501 --> 00:04:22,370
in your Program Files.

96
00:04:22,370 --> 00:04:24,462
It doesn't really matter where you put it.

97
00:04:24,462 --> 00:04:26,007
But, as I'm using Linux,

98
00:04:26,007 --> 00:04:29,883
I'm going to put it in a
folder called OPT Wildfly.

99
00:04:29,883 --> 00:04:32,135
I'll need to do this
using a terminal window

100
00:04:32,135 --> 00:04:34,581
so that I have the
permissions to do the move.

101
00:04:34,581 --> 00:04:37,441
I'll navigate to the Downloads folder.

102
00:04:38,978 --> 00:04:40,165
Let's do an LS check.

103
00:04:40,165 --> 00:04:42,782
This is the folder I'm trying
to move called Wildfly.

104
00:04:42,782 --> 00:04:47,782
So to move it, I'll need to do sudo, MV,

105
00:04:48,396 --> 00:04:53,396
and then I'm moving Wildfly
into OPT, I'll press Enter.

106
00:04:55,693 --> 00:04:57,737
And hopefully that's happened.

107
00:04:57,737 --> 00:05:01,322
Let's just move to the new
Wildfly folder to check.

108
00:05:02,875 --> 00:05:04,760
Yes, so there it is.

109
00:05:06,463 --> 00:05:09,699
And, in here, you'll see
there's a number of subfolders

110
00:05:09,699 --> 00:05:14,111
called bin, domain, standalone, and so on.

111
00:05:14,111 --> 00:05:17,979
Now that's all we actually
need to do to install Wildfly,

112
00:05:17,979 --> 00:05:20,472
but there is a configuration step,

113
00:05:20,472 --> 00:05:23,938
which is that we do need
to create a user account.

114
00:05:23,938 --> 00:05:26,836
Now, I'm going to be
using a terminal window

115
00:05:26,836 --> 00:05:28,949
to configure and run Wildfly

116
00:05:28,949 --> 00:05:31,409
and I suggest that you do the same.

117
00:05:31,409 --> 00:05:33,566
If you're using a Mac or a Linux machine,

118
00:05:33,566 --> 00:05:35,945
you'll need to, therefore,
open a terminal window.

119
00:05:35,945 --> 00:05:39,858
If you're using a Windows PC
then open up a command prompt.

120
00:05:39,858 --> 00:05:43,154
And then navigate to the Wildfly folder.

121
00:05:43,154 --> 00:05:44,837
So that's where I am now.

122
00:05:44,837 --> 00:05:46,305
If you do an LS,

123
00:05:46,305 --> 00:05:48,676
or if you're Windows
you'll need to type in DIR,

124
00:05:48,676 --> 00:05:52,730
you should see the folders
called bin, appclient, and so on.

125
00:05:52,730 --> 00:05:54,902
We'll need to be in the bin folder.

126
00:05:54,902 --> 00:05:57,344
So to do that, we'll do CD bin.

127
00:05:57,344 --> 00:05:59,106
And, again, I'm going to do an LS

128
00:05:59,106 --> 00:06:01,388
to get the list of files in this folder.

129
00:06:01,388 --> 00:06:03,183
And to create the user,

130
00:06:03,183 --> 00:06:06,859
we need to run the script called add-user.

131
00:06:06,859 --> 00:06:10,045
Now, if you're on a Windows computer,

132
00:06:10,045 --> 00:06:12,533
you're actually going to be
running this version here,

133
00:06:12,533 --> 00:06:15,186
which is add-user.bat.

134
00:06:15,186 --> 00:06:19,556
So if you're on Windows,
type in add-user.bat

135
00:06:19,556 --> 00:06:21,273
and press Enter.

136
00:06:21,273 --> 00:06:23,879
If you're on a Mac or a Linux machine,

137
00:06:23,879 --> 00:06:26,247
then you're going to be
using this version here,

138
00:06:26,247 --> 00:06:28,361
the version that ends in .sh.

139
00:06:28,361 --> 00:06:33,361
And to do that, we'll
enter a ./add-user.sh

140
00:06:34,083 --> 00:06:35,878
and then we'll get the same thing

141
00:06:35,878 --> 00:06:37,674
whether it's Windows or not.

142
00:06:37,674 --> 00:06:40,083
We want to select a management user

143
00:06:40,083 --> 00:06:43,635
so we can just press Enter
on this first prompt.

144
00:06:43,635 --> 00:06:45,962
Now, for the username and password,

145
00:06:45,962 --> 00:06:49,222
I'm going to just enter the
word admin in lowercase,

146
00:06:49,222 --> 00:06:52,289
so that it's easy for us to remember.

147
00:06:52,289 --> 00:06:54,934
When we get to the password prompt,

148
00:06:54,934 --> 00:06:56,450
I just need to put in a yes here

149
00:06:56,450 --> 00:06:59,266
to say we do want to
use the username admin.

150
00:06:59,266 --> 00:07:02,246
And for the password,
I'll do the same thing.

151
00:07:02,246 --> 00:07:05,460
Now, with a password,
we're going to be told

152
00:07:05,460 --> 00:07:08,275
that there's a problem that the
password should be different

153
00:07:08,275 --> 00:07:09,795
from the username.

154
00:07:09,795 --> 00:07:11,789
I'm going to be using the word admin

155
00:07:11,789 --> 00:07:13,922
as the password in lower case.

156
00:07:13,922 --> 00:07:15,591
We are allowed to do that

157
00:07:15,591 --> 00:07:17,352
even though we're told we really should

158
00:07:17,352 --> 00:07:19,105
use a stronger password,

159
00:07:19,105 --> 00:07:21,673
But this is absolutely fine
for development purposes.

160
00:07:21,673 --> 00:07:24,611
So just do the same as me and press yes.

161
00:07:24,611 --> 00:07:26,528
Of course, you could put
in a different password

162
00:07:26,528 --> 00:07:27,755
if you wish.

163
00:07:27,755 --> 00:07:30,193
So we'll reenter the password.

164
00:07:30,193 --> 00:07:32,446
Now, we don't need to
be part of any groups

165
00:07:32,446 --> 00:07:35,059
so you can just press
Enter at this prompt.

166
00:07:35,059 --> 00:07:37,874
And then we do want to be
in the Management Realm,

167
00:07:37,874 --> 00:07:40,605
so we'll type in the word yes.

168
00:07:40,605 --> 00:07:44,724
And then finally, this
final question is asking us

169
00:07:44,724 --> 00:07:48,098
if we want the user to be an account

170
00:07:48,098 --> 00:07:50,305
which can be used to connect together

171
00:07:50,305 --> 00:07:52,709
two different application servers.

172
00:07:52,709 --> 00:07:54,424
We're not going to be doing that,

173
00:07:54,424 --> 00:07:57,160
so we'll enter a no here.

174
00:07:57,160 --> 00:07:58,171
And that's it,

175
00:07:58,171 --> 00:07:59,728
that's our user created.

176
00:07:59,728 --> 00:08:03,475
So we're now ready to start
the application server.

177
00:08:03,475 --> 00:08:06,138
So we do that by entering the command,

178
00:08:06,138 --> 00:08:10,058
if you're on, Windows it's standalone.bat.

179
00:08:10,058 --> 00:08:11,935
Type that in and press Enter.

180
00:08:11,935 --> 00:08:16,935
If you're on Linux or a
Mac, it's ./standalone.sh.

181
00:08:19,187 --> 00:08:20,755
We'll press Enter.

182
00:08:22,136 --> 00:08:24,831
And we'll just wait for that to start up.

183
00:08:24,831 --> 00:08:27,237
OK, that didn't take very long

184
00:08:27,237 --> 00:08:29,524
and, hopefully, you're
screen looks like mine

185
00:08:29,524 --> 00:08:32,252
and you've got no errors here.

186
00:08:32,252 --> 00:08:35,644
Now I've switched temporarily
to a Windows machine,

187
00:08:35,644 --> 00:08:39,483
because sometimes when you
install Wildfly on Windows,

188
00:08:39,483 --> 00:08:42,788
you do get an error when
you try and start it.

189
00:08:42,788 --> 00:08:44,782
So I wanted to show you that error

190
00:08:44,782 --> 00:08:47,239
in case this has happened to you.

191
00:08:47,239 --> 00:08:49,678
When we look at the output to our console,

192
00:08:49,678 --> 00:08:51,521
we can see at the bottom here

193
00:08:51,521 --> 00:08:53,359
that the word error has appeared,

194
00:08:53,359 --> 00:08:57,235
although actually the
Wildfly service has started.

195
00:08:57,235 --> 00:09:01,072
If we go up, we'll see
what's gone wrong is up here.

196
00:09:01,072 --> 00:09:03,840
And that is that we've
not managed to start

197
00:09:03,840 --> 00:09:07,387
what's called the http-interface service

198
00:09:07,387 --> 00:09:10,172
and the reason for that is the port

199
00:09:10,172 --> 00:09:14,499
that the http-interface
service uses is already in use.

200
00:09:14,499 --> 00:09:18,907
That is port number 9990.

201
00:09:18,907 --> 00:09:21,391
Now the reason why this
sometimes happens particularly

202
00:09:21,391 --> 00:09:26,391
on Windows is that if you're
using Nvidia display drivers,

203
00:09:26,730 --> 00:09:31,148
well, they come with a service
which checks for updates

204
00:09:31,148 --> 00:09:33,791
to the Nvidia software and, unfortunately,

205
00:09:33,791 --> 00:09:38,406
that service uses the same port, 9990.

206
00:09:38,406 --> 00:09:42,159
So, if you are using Windows
and you get this message,

207
00:09:42,159 --> 00:09:46,650
what you need to do is disable
that service from running.

208
00:09:46,650 --> 00:09:48,286
Now that's safe to do.

209
00:09:48,286 --> 00:09:50,528
All the service does is check for updates.

210
00:09:50,528 --> 00:09:52,928
You might want to temporarily stop it

211
00:09:52,928 --> 00:09:55,866
or, of course, you can always
check for updates manually.

212
00:09:55,866 --> 00:09:58,436
But just in case you're not
familiar with how to do that,

213
00:09:58,436 --> 00:10:00,354
I'll do that now.

214
00:10:00,354 --> 00:10:03,132
So we'll start by opening
up Windows Explorer,

215
00:10:03,132 --> 00:10:07,579
and we need to right click on
Computer and choose Manage.

216
00:10:07,579 --> 00:10:09,213
On this window that appears,

217
00:10:09,213 --> 00:10:13,093
we can expand the left
hand side, choose Services,

218
00:10:13,093 --> 00:10:15,500
and then we need to scroll down this list

219
00:10:15,500 --> 00:10:19,954
till we find the one called
Nvidia Network Service.

220
00:10:21,209 --> 00:10:22,477
So here it is here,

221
00:10:22,477 --> 00:10:24,147
I'll just make this column a bit bigger

222
00:10:24,147 --> 00:10:26,398
so you can see Nvidia Network Service.

223
00:10:26,398 --> 00:10:28,396
And I'm just going to click on Stop

224
00:10:28,396 --> 00:10:30,434
to stop the service from running.

225
00:10:30,434 --> 00:10:32,273
When my machine reboots

226
00:10:32,273 --> 00:10:34,967
that service will automatically restart,

227
00:10:34,967 --> 00:10:37,252
so I don't have to worry about my drivers

228
00:10:37,252 --> 00:10:38,671
getting out of date,

229
00:10:38,671 --> 00:10:40,270
but at least it means for now

230
00:10:40,270 --> 00:10:41,982
that service isn't running,

231
00:10:41,982 --> 00:10:44,795
so Wildfly should be able to start OK.

232
00:10:44,795 --> 00:10:46,501
So having stopped that service,

233
00:10:46,501 --> 00:10:48,387
we can go back to our console.

234
00:10:48,387 --> 00:10:50,605
We'll need to press Control and C

235
00:10:50,605 --> 00:10:52,415
to stop the application running,

236
00:10:52,415 --> 00:10:54,578
and Y to terminate the batch job.

237
00:10:54,578 --> 00:10:59,075
And then we can reissue the
command to standalone.bat.

238
00:10:59,075 --> 00:11:00,546
And we'll just give
this a moment and check

239
00:11:00,546 --> 00:11:02,907
that it starts up OK this time.

240
00:11:02,907 --> 00:11:04,581
OK, and it has done.

241
00:11:04,581 --> 00:11:06,171
We've got purely infos,

242
00:11:06,171 --> 00:11:08,332
there are no errors reported.

243
00:11:08,332 --> 00:11:11,103
I'm now going to switch
back to my normal machine

244
00:11:11,103 --> 00:11:14,193
and I want to point
out at the bottom here,

245
00:11:14,193 --> 00:11:17,469
you'll see that not every part of Wildfly

246
00:11:17,469 --> 00:11:20,235
is actually running just yet.

247
00:11:20,235 --> 00:11:25,184
What we can see here is
that there are currently 267

248
00:11:25,184 --> 00:11:28,813
of the 553 services that are running.

249
00:11:28,813 --> 00:11:31,338
The others are sat there in the background

250
00:11:31,338 --> 00:11:33,435
waiting to be started.

251
00:11:33,435 --> 00:11:35,353
This isn't anything to worry about.

252
00:11:35,353 --> 00:11:37,962
It's a feature of Wildfly that allows it

253
00:11:37,962 --> 00:11:40,001
to start more quickly.

254
00:11:40,001 --> 00:11:43,300
Wildfly only starts the services it needs

255
00:11:43,300 --> 00:11:46,443
for the applications that have
been deployed to the server.

256
00:11:46,443 --> 00:11:48,330
As we haven't deployed any yet,

257
00:11:48,330 --> 00:11:50,614
there are a lot of services
that aren't running,

258
00:11:50,614 --> 00:11:53,551
but they'll get started as
and when they're needed.

259
00:11:53,551 --> 00:11:55,790
So now that our server is up and running,

260
00:11:55,790 --> 00:11:57,912
we can look at its management console

261
00:11:57,912 --> 00:11:59,820
to check that everything's working.

262
00:11:59,820 --> 00:12:02,195
So to do this, go to your browser,

263
00:12:02,195 --> 00:12:05,664
and we want to go to the URL

264
00:12:05,664 --> 00:12:09,957
of localhost:9990.

265
00:12:11,994 --> 00:12:15,097
Now, we'll now be prompted for the login

266
00:12:15,097 --> 00:12:18,360
and that's the user that we
created a few moments ago.

267
00:12:18,360 --> 00:12:21,257
So for me that was a username of admin

268
00:12:21,257 --> 00:12:23,907
and a password of admin.

269
00:12:23,907 --> 00:12:25,460
And we'll click on OK.

270
00:12:25,460 --> 00:12:27,500
And I'm going to choose
to remember that username

271
00:12:27,500 --> 00:12:30,224
and password so I don't have
to keep editing it every time.

272
00:12:30,224 --> 00:12:32,671
And if you can see a screen
that looks like this,

273
00:12:32,671 --> 00:12:36,298
then well done, you've
successfully got Wildfly installed

274
00:12:36,298 --> 00:12:38,191
and it's running.

