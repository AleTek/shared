1
00:00:00,011 --> 00:00:02,867
(upbeat electronic music)

2
00:00:02,867 --> 00:00:05,641
- [Voiceover] Welcome to
the JavaEE training course.

3
00:00:05,641 --> 00:00:07,691
I'm Matt Greencroft, one of the trainers

4
00:00:07,691 --> 00:00:09,326
at Virtual Pair Programmers,

5
00:00:09,326 --> 00:00:12,152
and I'll be taking you
through this first module

6
00:00:12,152 --> 00:00:14,542
on Enterprise Java with Wildfly,

7
00:00:14,542 --> 00:00:18,999
which is approximately five
and a half hours of training.

8
00:00:18,999 --> 00:00:20,667
By the end of this module,

9
00:00:20,667 --> 00:00:23,187
we'll have built together
a fully functionining

10
00:00:23,187 --> 00:00:26,311
production standard end-to-end system.

11
00:00:26,311 --> 00:00:28,813
We'll have a client which
connects to a server,

12
00:00:28,813 --> 00:00:30,793
which talks to a database.

13
00:00:31,999 --> 00:00:35,558
I plan to produce two further
modules in this course.

14
00:00:35,558 --> 00:00:37,017
In the second module,

15
00:00:37,017 --> 00:00:39,522
we'll be looking at other more modern ways

16
00:00:39,522 --> 00:00:42,176
to communicate between
a server and a client

17
00:00:42,176 --> 00:00:43,981
using web services.

18
00:00:43,981 --> 00:00:46,416
We'll also look at using message queues

19
00:00:46,416 --> 00:00:48,190
in the second module.

20
00:00:48,190 --> 00:00:52,131
Then in Module 3, we'll
be looking at using JavaEE

21
00:00:52,131 --> 00:00:53,809
to create web pages

22
00:00:53,809 --> 00:00:55,990
and we'll also cover how to secure

23
00:00:55,990 --> 00:00:58,929
your JavaEE applications.

24
00:00:58,929 --> 00:01:00,663
Before I go any further,

25
00:01:00,663 --> 00:01:03,304
I should say that this
is the second iteration

26
00:01:03,304 --> 00:01:07,165
of Virtual Pair Programmers'
JavaEE training.

27
00:01:07,165 --> 00:01:11,872
We created the first version
of this course using JavaEE 6

28
00:01:11,872 --> 00:01:14,369
and that version used
an application server

29
00:01:14,369 --> 00:01:16,147
called GlassFish.

30
00:01:16,147 --> 00:01:18,917
Now although at the time
of writing this course,

31
00:01:18,917 --> 00:01:22,908
the latest verson of JavaEE is JavaEE 7,

32
00:01:22,908 --> 00:01:24,841
almost all of the original course

33
00:01:24,841 --> 00:01:27,485
is still valid and still works.

34
00:01:27,485 --> 00:01:30,643
However, we decided to replace
it with this new version

35
00:01:30,643 --> 00:01:33,429
as there are a couple of
techniques that have changed

36
00:01:33,429 --> 00:01:34,875
in the latest version.

37
00:01:34,875 --> 00:01:37,938
But more importantly the
newer versions of GlassFish

38
00:01:37,938 --> 00:01:40,418
have some rather horrible bugs in them,

39
00:01:40,418 --> 00:01:43,755
which led to some real
difficulties for our students.

40
00:01:43,755 --> 00:01:45,357
So in this version of the course,

41
00:01:45,357 --> 00:01:46,638
we've chosen to work with

42
00:01:46,638 --> 00:01:49,657
a different application
server called WildFly.

43
00:01:49,657 --> 00:01:51,070
But if you've already studied

44
00:01:51,070 --> 00:01:53,143
the previous version of this course,

45
00:01:53,143 --> 00:01:56,865
then you'll find that most
of the content is identical.

46
00:01:56,865 --> 00:01:58,673
There's very little that has changed

47
00:01:58,673 --> 00:02:00,610
in this version of JavaEE.

48
00:02:01,614 --> 00:02:03,237
Everything we do in the course

49
00:02:03,237 --> 00:02:05,947
will be to professional standards.

50
00:02:05,947 --> 00:02:08,397
We'll be using a very
simple set of requirements

51
00:02:08,397 --> 00:02:10,968
for our project just to keep things easy.

52
00:02:10,968 --> 00:02:13,493
But we'll be creating fully functioning

53
00:02:13,493 --> 00:02:15,105
production-quality code

54
00:02:15,105 --> 00:02:17,726
so that you'll be gaining
the skills you need

55
00:02:17,726 --> 00:02:21,692
to work on real live
developmment projects.

56
00:02:21,692 --> 00:02:23,719
Although we'll be using WildFly

57
00:02:23,719 --> 00:02:25,776
as our application server on this course,

58
00:02:25,776 --> 00:02:28,900
all of the course is also
suitable for other servers,

59
00:02:28,900 --> 00:02:31,770
such as WebSphere or GlassFish.

60
00:02:31,770 --> 00:02:34,969
Before we start, I just need
to check that you're okay

61
00:02:34,969 --> 00:02:37,163
with the prerequisites for this course.

62
00:02:37,163 --> 00:02:39,569
I'm assuming that you
have reasonable knowledge

63
00:02:39,569 --> 00:02:40,918
of standard Java.

64
00:02:40,918 --> 00:02:43,420
You'll need to be comfortable
with writing classes,

65
00:02:43,420 --> 00:02:46,650
creating objects and
the basic Java syntax.

66
00:02:46,650 --> 00:02:49,626
If you've studied our Java
fundamentals training,

67
00:02:49,626 --> 00:02:52,917
then you'll certainly
have all that you need.

68
00:02:52,917 --> 00:02:56,390
In later chapters we'll
be working with a database

69
00:02:56,390 --> 00:02:58,691
and we'll be using JPA,

70
00:02:58,691 --> 00:03:01,382
the Java Persistence API.

71
00:03:01,382 --> 00:03:03,797
We have a full course all about JPA

72
00:03:03,797 --> 00:03:05,607
here at Virtual Pair Programmers,

73
00:03:05,607 --> 00:03:08,975
and I've only touched on
the basics in this course.

74
00:03:08,975 --> 00:03:11,230
So if you're not familiar with JPA,

75
00:03:11,230 --> 00:03:12,965
well, firstly don't worry,

76
00:03:12,965 --> 00:03:14,599
you'll be able to follow this course,

77
00:03:14,599 --> 00:03:17,659
and then maybe take the
JPA course later on.

78
00:03:17,659 --> 00:03:22,001
I've not assumed any knowledge
of JPA for this course.

79
00:03:22,001 --> 00:03:24,544
We also use a build script in this course,

80
00:03:24,544 --> 00:03:27,853
but again I've not assumed any
knowledge of build scripts,

81
00:03:27,853 --> 00:03:30,386
but we do have a Java build tools course

82
00:03:30,386 --> 00:03:31,928
here at Virtual Pair Programmers

83
00:03:31,928 --> 00:03:33,129
that you might want to study

84
00:03:33,129 --> 00:03:35,743
to learn more about this topic later on.

85
00:03:36,692 --> 00:03:38,513
Now we have worked really hard

86
00:03:38,513 --> 00:03:39,976
as we prepared this course

87
00:03:39,976 --> 00:03:43,645
to produce what I hope is going
to be an error-free course.

88
00:03:43,645 --> 00:03:47,213
But naturally a few mistakes
might creep in along the way

89
00:03:47,213 --> 00:03:50,610
so please do regularly check
the webpage for this course

90
00:03:50,610 --> 00:03:53,214
at virtualpairprogrammers.com.

91
00:03:53,214 --> 00:03:56,150
You'll see a link there for
the errata for this course,

92
00:03:56,150 --> 00:03:59,264
which I'll update with any known problems.

93
00:03:59,264 --> 00:04:01,279
In a moment I'll explain how

94
00:04:01,279 --> 00:04:03,314
this first module is structured.

95
00:04:03,314 --> 00:04:05,581
But before that, I think
it might be sensible

96
00:04:05,581 --> 00:04:07,985
to talk about what JavaEE is

97
00:04:07,985 --> 00:04:11,140
and how it differs from standard Java.

98
00:04:11,140 --> 00:04:13,420
Towards the end of the 1990s,

99
00:04:13,420 --> 00:04:16,160
Java became popular as
a programming language

100
00:04:16,160 --> 00:04:18,877
for writing server side systems.

101
00:04:18,877 --> 00:04:20,665
That includes, for example,

102
00:04:20,665 --> 00:04:23,730
creating logic to control
complex web applications,

103
00:04:23,730 --> 00:04:27,240
and the server side of
client-server applications.

104
00:04:27,240 --> 00:04:29,246
In fact, if you're going to develop

105
00:04:29,246 --> 00:04:31,395
a server application today,

106
00:04:31,395 --> 00:04:33,252
then you really will be choosing between

107
00:04:33,252 --> 00:04:37,221
either Java or Microsoft's .Net platform.

108
00:04:37,221 --> 00:04:39,383
There are other server technologies.

109
00:04:39,383 --> 00:04:42,146
You can create websites
in all sorts of languages,

110
00:04:42,146 --> 00:04:44,038
and some of the JavaScript frameworks

111
00:04:44,038 --> 00:04:46,638
have been written to
create server applications.

112
00:04:46,638 --> 00:04:49,448
But most larger
applications will be written

113
00:04:49,448 --> 00:04:52,393
in either .Net or Java.

114
00:04:52,393 --> 00:04:55,927
So this course is all about
writing programs in Java

115
00:04:55,927 --> 00:04:58,109
to run on a server.

116
00:04:58,109 --> 00:05:00,534
As you'll see the Java that we'll write

117
00:05:00,534 --> 00:05:04,407
is the same Java that we
use in everyday programming.

118
00:05:04,407 --> 00:05:06,345
We'll still be creating classes

119
00:05:06,345 --> 00:05:07,708
and we'll still be using

120
00:05:07,708 --> 00:05:11,387
the so-called standard
edition Java libraries.

121
00:05:11,387 --> 00:05:13,690
All the syntax is the same for loops,

122
00:05:13,690 --> 00:05:17,340
if statements, interfaces,
classes and so on.

123
00:05:17,340 --> 00:05:19,221
But we're going to need more than that

124
00:05:19,221 --> 00:05:22,472
to be able to write server side programs.

125
00:05:22,472 --> 00:05:25,275
We're going to need a
server to run our code

126
00:05:25,275 --> 00:05:26,904
and we're going to need that server

127
00:05:26,904 --> 00:05:30,214
to provide certain services to us.

128
00:05:30,214 --> 00:05:33,548
So what does a server
need to be able to do?

129
00:05:33,548 --> 00:05:36,288
Well, first and foremost,
for many projects

130
00:05:36,288 --> 00:05:40,570
the obvious answer will be it
needs to provide a database.

131
00:05:40,570 --> 00:05:42,954
We're going to need to store our data

132
00:05:42,954 --> 00:05:46,438
in some kind of persistent
storage mechanism.

133
00:05:46,438 --> 00:05:49,503
Now to do that, we're
going to need software.

134
00:05:49,503 --> 00:05:51,523
Obviously, we'll need a database,

135
00:05:51,523 --> 00:05:53,248
but we'll also going to need

136
00:05:53,248 --> 00:05:55,940
some kind of bridge to the database

137
00:05:55,940 --> 00:05:58,099
so that we can write Java code

138
00:05:58,099 --> 00:05:59,957
that will be able to insert data

139
00:05:59,957 --> 00:06:04,075
into database tables and to
query the database and so on.

140
00:06:04,941 --> 00:06:06,364
If we assume that we're going to be

141
00:06:06,364 --> 00:06:08,258
writing an application for the web,

142
00:06:08,258 --> 00:06:11,221
as we will be in the third
module of this course,

143
00:06:11,221 --> 00:06:13,424
then we're going to need
some kind of software

144
00:06:13,424 --> 00:06:15,452
to serve the webpages.

145
00:06:15,452 --> 00:06:17,588
We'll need a web server.

146
00:06:17,588 --> 00:06:20,026
When we're dealing with
transactions on the database,

147
00:06:20,026 --> 00:06:22,849
we're going to need software
to support that as well.

148
00:06:22,849 --> 00:06:25,503
This is a called transaction manager.

149
00:06:25,503 --> 00:06:28,300
Now don't worry if you don't
know what transactions are

150
00:06:28,300 --> 00:06:30,313
or why we need software to do this.

151
00:06:30,313 --> 00:06:33,915
This is going to be explained
in detail in a later chapter.

152
00:06:33,915 --> 00:06:35,975
But it's sufficient to say for the moment

153
00:06:35,975 --> 00:06:39,259
that transaction management
is quite a complicated job

154
00:06:39,259 --> 00:06:41,551
that the server needs to do.

155
00:06:41,551 --> 00:06:45,390
We might also need to secure
a server side application

156
00:06:45,390 --> 00:06:49,283
so that users need to log in
before they can do anything.

157
00:06:49,283 --> 00:06:51,025
This kind of feature isn't present

158
00:06:51,025 --> 00:06:54,372
in the standard Java libraries
that you might be used to.

159
00:06:54,372 --> 00:06:56,754
So we're going to need
some software on our server

160
00:06:56,754 --> 00:06:58,562
to do this for us.

161
00:06:59,781 --> 00:07:04,632
For many years now software
vendors such as Oracle and IBM

162
00:07:04,632 --> 00:07:06,871
have been providing software packages

163
00:07:06,871 --> 00:07:08,906
called application servers.

164
00:07:08,906 --> 00:07:11,391
It's a slightly misleading term really.

165
00:07:11,391 --> 00:07:14,062
An application server
isn't a piece of hardware,

166
00:07:14,062 --> 00:07:15,800
it's a software product.

167
00:07:15,800 --> 00:07:17,823
What an application server does

168
00:07:17,823 --> 00:07:19,999
is it provides exactly the services

169
00:07:19,999 --> 00:07:21,445
that I've just described,

170
00:07:21,445 --> 00:07:23,321
a database, a web server,

171
00:07:23,321 --> 00:07:25,981
a transaction manager, a security system,

172
00:07:25,981 --> 00:07:29,649
and actually many more features as well.

173
00:07:29,649 --> 00:07:30,902
Now for many projects,

174
00:07:30,902 --> 00:07:33,394
an application server might be overkill.

175
00:07:33,394 --> 00:07:37,056
Certainly some application
servers are very expensive.

176
00:07:37,056 --> 00:07:38,834
Some projects could serve me well

177
00:07:38,834 --> 00:07:40,927
just use a basic web server.

178
00:07:40,927 --> 00:07:43,780
But many projects and
especially corporate projects

179
00:07:43,780 --> 00:07:46,596
will like the fact that
application servers are big

180
00:07:46,596 --> 00:07:50,264
and can handle a wide
variety of requirements.

181
00:07:50,264 --> 00:07:52,875
Also most application server vendors

182
00:07:52,875 --> 00:07:55,383
also provide support, consultancy,

183
00:07:55,383 --> 00:07:57,758
and insurance against problems,

184
00:07:57,758 --> 00:08:00,650
which big companies often appreciate.

185
00:08:00,650 --> 00:08:02,338
So an application server provides

186
00:08:02,338 --> 00:08:05,466
a variety of software tools and services

187
00:08:05,466 --> 00:08:09,209
that we can use when we're
writing server side software.

188
00:08:09,209 --> 00:08:12,582
That's great, but this did
lead to a serious problem

189
00:08:12,582 --> 00:08:14,754
through the late 1990s

190
00:08:14,754 --> 00:08:16,738
as many different software vendors

191
00:08:16,738 --> 00:08:19,235
were producing these application servers.

192
00:08:19,235 --> 00:08:21,721
Every vendor had a different feature set

193
00:08:21,721 --> 00:08:25,467
and every vendor had a
different set of APIs.

194
00:08:26,602 --> 00:08:30,209
By the way, if you haven't
heard of the term API before,

195
00:08:30,209 --> 00:08:33,834
this stands for application
programming interface.

196
00:08:33,834 --> 00:08:38,050
It just means the interface
to a set of libraries.

197
00:08:38,050 --> 00:08:39,670
As a Java developer,

198
00:08:39,670 --> 00:08:42,971
we're very familiar with
the standard Java API.

199
00:08:42,971 --> 00:08:44,271
That's where we get the objects

200
00:08:44,271 --> 00:08:46,802
like string and array list from.

201
00:08:46,802 --> 00:08:49,473
Well, when working on
an application server,

202
00:08:49,473 --> 00:08:52,133
they would provide a
whole new set of libraries

203
00:08:52,133 --> 00:08:54,668
that you would have to learn the API for.

204
00:08:54,668 --> 00:08:56,432
But that meant that if I was working

205
00:08:56,432 --> 00:08:59,182
on an application server from IBM, say,

206
00:08:59,182 --> 00:09:02,061
I'd have to learn the IBM API.

207
00:09:02,061 --> 00:09:04,529
But then if I moved
companies onto a new project

208
00:09:04,529 --> 00:09:07,535
and they were using an
application server from Oracle,

209
00:09:07,535 --> 00:09:10,466
then I'd like have to learn
a whole new different API.

210
00:09:10,466 --> 00:09:11,651
Now this would be okay

211
00:09:11,651 --> 00:09:14,396
if there was just one
big vendor dominating

212
00:09:14,396 --> 00:09:18,727
and everyone in the Java
community used that one server.

213
00:09:18,727 --> 00:09:22,043
That's the position that
Microsoft programmers are in.

214
00:09:22,043 --> 00:09:23,918
But the Java community recognized

215
00:09:23,918 --> 00:09:26,721
that this vendor lock-in
was a very bad thing,

216
00:09:26,721 --> 00:09:30,861
and that could eventually lead
to Java failing altogether.

217
00:09:30,861 --> 00:09:32,665
So the vendors got together

218
00:09:32,665 --> 00:09:36,012
and formed a new standard
for application servers.

219
00:09:36,012 --> 00:09:38,168
The idea would be that if a vendor

220
00:09:38,168 --> 00:09:40,536
wanted to produce an application server,

221
00:09:40,536 --> 00:09:43,814
and they wanted the Java
community's stamp of approval,

222
00:09:43,814 --> 00:09:45,993
then they would have to do two things.

223
00:09:45,993 --> 00:09:50,033
They would have to implement
a specific feature set,

224
00:09:50,033 --> 00:09:51,980
so they would as a minimum provide, say,

225
00:09:51,980 --> 00:09:54,376
a web server, a transaction manager,

226
00:09:54,376 --> 00:09:56,728
a security system, and so on,

227
00:09:56,728 --> 00:09:58,596
but perhaps more importantly,

228
00:09:58,596 --> 00:10:02,042
they would also have to
provide a standard API

229
00:10:02,042 --> 00:10:04,917
for developers to use these features.

230
00:10:04,917 --> 00:10:08,680
So now Java programmers
could learn just one API

231
00:10:08,680 --> 00:10:11,489
and then they'd be able to
move from application server

232
00:10:11,489 --> 00:10:14,229
to application server
without having to learn

233
00:10:14,229 --> 00:10:16,969
a whole heap of new libraries.

234
00:10:16,969 --> 00:10:20,582
This standard was called J2EE,

235
00:10:20,582 --> 00:10:24,809
otherwise known as Java
2 Enterprise Edition.

236
00:10:24,809 --> 00:10:28,021
Remember that I'm talking
about the late 1990s here.

237
00:10:28,021 --> 00:10:30,324
You might remember that Java in those days

238
00:10:30,324 --> 00:10:32,757
was officially called Java 2.

239
00:10:33,831 --> 00:10:36,339
The Enterprise Edition
means, well to be honest,

240
00:10:36,339 --> 00:10:38,312
it doesn't really mean anything.

241
00:10:38,312 --> 00:10:41,575
For me, a better term
would've been server edition

242
00:10:41,575 --> 00:10:43,309
because that's what it is.

243
00:10:43,309 --> 00:10:44,945
It's a set of libraries

244
00:10:44,945 --> 00:10:48,210
to enable us to write server side code.

245
00:10:48,210 --> 00:10:51,921
Enterprise is really one of
those corporate jargon terms

246
00:10:51,921 --> 00:10:54,144
that doesn't mean very much.

247
00:10:54,144 --> 00:10:56,967
Now since then things have
moved on a little bit.

248
00:10:56,967 --> 00:10:58,827
The 2 has gone out of fashion,

249
00:10:58,827 --> 00:11:02,979
so officially the topic
we're studying is JavaEE,

250
00:11:02,979 --> 00:11:05,742
the Java Enterprise Edition.

251
00:11:05,742 --> 00:11:07,634
The latest version at the time

252
00:11:07,634 --> 00:11:10,177
of recording this course is version 7,

253
00:11:10,177 --> 00:11:13,123
and that's what this
course will be covering.

254
00:11:13,123 --> 00:11:16,284
So JavaEE is quite a strange thing.

255
00:11:16,284 --> 00:11:17,677
It's not a new language.

256
00:11:17,677 --> 00:11:20,647
It's not a new form of Java
that you have to pay for.

257
00:11:20,647 --> 00:11:24,624
It's just a set of libraries
that allow us to write code

258
00:11:24,624 --> 00:11:28,801
that will run on any
compliant application server.

259
00:11:28,801 --> 00:11:32,267
But that does mean that in
order to write JavaEE code,

260
00:11:32,267 --> 00:11:34,930
you do need an application server.

261
00:11:34,930 --> 00:11:36,300
In the next chapter,

262
00:11:36,300 --> 00:11:38,234
we're going to download, install,

263
00:11:38,234 --> 00:11:40,981
and configure one of
these application servers.

264
00:11:40,981 --> 00:11:43,214
We'll be using what I believe is currently

265
00:11:43,214 --> 00:11:45,280
the most popular application server,

266
00:11:45,280 --> 00:11:48,164
which is WildFly from JBoss.

267
00:11:48,164 --> 00:11:50,307
I'm going to be using
Eclipse on the videos,

268
00:11:50,307 --> 00:11:53,703
but you can use any development
environment you like.

269
00:11:53,703 --> 00:11:56,045
I won't be relying on any special tools.

270
00:11:56,045 --> 00:11:57,690
I'm going to do everything by hand

271
00:11:57,690 --> 00:12:01,022
so that we understand
everything that we need to know.

272
00:12:01,022 --> 00:12:03,373
In Chapter 3, we'll learn all about

273
00:12:03,373 --> 00:12:06,484
Enterprise Java Beans or EJB.

274
00:12:06,484 --> 00:12:08,510
We're going to build on
the work in this chapter

275
00:12:08,510 --> 00:12:10,617
throughout the course
so you're going to learn

276
00:12:10,617 --> 00:12:14,315
that EJBs are central to JavaEE.

277
00:12:14,315 --> 00:12:16,939
In Chapter 4, we'll study JNDI.

278
00:12:16,939 --> 00:12:18,834
This will allow us to access the server

279
00:12:18,834 --> 00:12:21,123
from clients all over the world.

280
00:12:21,123 --> 00:12:24,861
Knowledge of JNDI is useful
in a lot of different areas

281
00:12:24,861 --> 00:12:26,625
of Enterprise Java.

282
00:12:26,625 --> 00:12:29,131
We'll build our first
client in this chapter,

283
00:12:29,131 --> 00:12:32,407
and we'll create a
connection to the server.

284
00:12:32,407 --> 00:12:36,006
In Chapter 5, we'll need
another chapter on EJB.

285
00:12:36,006 --> 00:12:38,726
This advanced chapter will
give you a good understanding

286
00:12:38,726 --> 00:12:41,788
of how EJBs are used in real projects.

287
00:12:41,788 --> 00:12:44,552
We'll learn about local and remote EJBs

288
00:12:44,552 --> 00:12:46,843
and stateful beans.

289
00:12:46,843 --> 00:12:48,637
Chapter 6 goes a little further

290
00:12:48,637 --> 00:12:51,149
in how EJBs can be connected together

291
00:12:51,149 --> 00:12:53,789
using context and dependency injection.

292
00:12:54,840 --> 00:12:57,007
Then in Chapter 7 we'll start to use

293
00:12:57,007 --> 00:12:58,990
databases for the first time.

294
00:12:58,990 --> 00:13:02,261
In this chapter, we'll
introduce the JPA library,

295
00:13:02,261 --> 00:13:05,122
or the Java Persistence API.

296
00:13:05,122 --> 00:13:09,255
Chapter 8 shows you how to
use JPA on the WildFly server,

297
00:13:09,255 --> 00:13:11,020
and this is a big part of the course

298
00:13:11,020 --> 00:13:13,502
because really from here
you'll start to build

299
00:13:13,502 --> 00:13:16,055
a more serious architecture.

300
00:13:16,055 --> 00:13:18,390
Chapter 9 talks about transactions.

301
00:13:18,390 --> 00:13:20,238
This is one of the few
chapters on the course

302
00:13:20,238 --> 00:13:21,870
where there's a lot of theory,

303
00:13:21,870 --> 00:13:24,265
a lot of talking, and not much code.

304
00:13:24,265 --> 00:13:26,251
But there are lots of
very important things

305
00:13:26,251 --> 00:13:28,258
you'll need to know here.

306
00:13:28,258 --> 00:13:30,969
Chapter 10 continues with the same topic.

307
00:13:30,969 --> 00:13:34,663
This time we'll be looking at
rollbacks on a JavaEE server,

308
00:13:34,663 --> 00:13:36,562
what to do when things go wrong.

309
00:13:37,973 --> 00:13:41,177
Before we start Chapter
2 I want to just mention

310
00:13:41,177 --> 00:13:44,703
our case study that we'll be
working through in this course.

311
00:13:44,703 --> 00:13:45,828
We're going to be building

312
00:13:45,828 --> 00:13:48,701
a simple employee management system.

313
00:13:48,701 --> 00:13:51,211
In this module all the system will be

314
00:13:51,211 --> 00:13:52,920
is a way of recording information

315
00:13:52,920 --> 00:13:55,153
about our company's employees.

316
00:13:55,153 --> 00:13:57,256
We'll have features to add, edit,

317
00:13:57,256 --> 00:14:00,029
and delete an employee, for example.

318
00:14:00,029 --> 00:14:01,836
I'm keeping the case study simple

319
00:14:01,836 --> 00:14:04,636
so that we can concentrate on
the code that we're writing.

320
00:14:04,636 --> 00:14:07,828
But this is going to be a great
template for many projects

321
00:14:07,828 --> 00:14:09,898
that you'll be creating as you continue

322
00:14:09,898 --> 00:14:12,427
developing JavaEE code.

323
00:14:12,427 --> 00:14:14,079
So let's get started.

324
00:14:14,079 --> 00:14:15,709
I'll see you in Chapter 2

325
00:14:15,709 --> 00:14:19,278
where we'll obtain and install
our application server.

326
00:14:19,278 --> 00:14:22,797
(update electronic music)

